$(document).ready(function()
{
   skrollr.init({forceHeight: false, mobileCheck: function() { return false; }, smoothScrolling: false});
   $("a[href*='#home']").click(function(event)
   {
      event.preventDefault();
      $('html, body').stop().animate({ scrollTop: $('#home').offset().top }, 600, 'easeInQuad');
   });
   $("a[href*='#contact']").click(function(event)
   {
      event.preventDefault();
      $('html, body').stop().animate({ scrollTop: $('#wb_contact').offset().top-80 }, 600, 'easeInQuad');
   });
   function MaterialIcon5Scroll()
   {
      var $obj = $("#wb_MaterialIcon5");
      if (!$obj.hasClass("in-viewport") && $obj.inViewPort(true))
      {
         $obj.addClass("in-viewport");
         AnimateCss('MaterialIcon1', 'animate-fade-in-up', 500, 1000);
      }
      else
      if ($obj.hasClass("in-viewport") && !$obj.inViewPort(true))
      {
         $obj.removeClass("in-viewport");
         AnimateCss('MaterialIcon1', 'animate-fade-out', 0, 0);
      }
   }
   if (!$('#wb_MaterialIcon5').inViewPort(true))
   {
      $('#wb_MaterialIcon5').addClass("in-viewport");
   }
   MaterialIcon5Scroll();
   $(window).scroll(function(event)
   {
      MaterialIcon5Scroll();
   });
   var jQueryDatePicker1Options =
   {
      dateFormat: 'mm/dd/yy',
      changeMonth: false,
      changeYear: false,
      showButtonPanel: false,
      showAnim: 'show'
   };
   $("#jQueryDatePicker1").datepicker(jQueryDatePicker1Options);
   $("#jQueryDatePicker1").datepicker("setDate", "");
   $("#jQueryDatePicker1").change(function()
   {
      $('#jQueryDatePicker1_input').attr('value',$(this).val());
   });
   var AutoComplete1Data = ["Bandung", "Jakarta", "Medan"];
   var AutoComplete1Options =
   {
      source: AutoComplete1Data,
      classes: { 'ui-autocomplete' : 'AutoComplete1' },
      delay: 0,
      minLength: 2
   };
   $("#AutoComplete1").autocomplete(AutoComplete1Options);
   var AutoComplete2Data = ["Bandung", "Jakarta", "Medan"];
   var AutoComplete2Options =
   {
      source: AutoComplete2Data,
      classes: { 'ui-autocomplete' : 'AutoComplete2' },
      delay: 0,
      minLength: 2
   };
   $("#AutoComplete2").autocomplete(AutoComplete2Options);
});
